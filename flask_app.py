"""
Used by pythonanywhere WSGI app to run site.
"""
import sys

path = '/home/kaamin/bid_analysis/'

if path not in sys.path:
   sys.path.insert(0, path)

import app


if __name__ == '__main__':
    app = app.create_app('default')
    app.run(debug=False, host='0.0.0.0')
