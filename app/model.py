import datetime
from peewee import (SqliteDatabase, Model, ForeignKeyField, 
    CharField, DateTimeField, IntegerField, FloatField, CompositeKey,
    JOIN, fn)

from . import db


DATE_FORMAT = "%m/%d/%Y"

class BaseModel(Model):
    """
    Defines base database class.
    """
    class Meta:
        database = db


class Unit(BaseModel):
    """
    Define table representing commonly used units in bids.
    """
    name = CharField()
    description = CharField()

    @staticmethod
    def insert_initial_data():
        unit_definitions = {
            'ACRE': 'area, acre',
            'C.Y.': 'volume, cubic yard',
            'EACH': 'each',
            'GAL': 'gallons',
            'L.S.': 'lump sum',
            'L.F.': 'lineal feet',
            'LBS.': 'pounds',
            'PAIR': 'pair',
            'S.F.': 'square feet',
            'S.Y.': 'square yard',
            'TONS': 'ton'
        }

        for name, description in unit_definitions.items():
            Unit.get_or_create(name=name, description=description)
    
    @classmethod
    def ac(self):
        """
        Shorthand for getting instance of ACRE
        """
        return self.get(self.name=='ACRE')
    
    @classmethod
    def cy(self):
        """
        Shorthand for getting instance of cubic yards
        """
        return self.get(self.name=='C.Y.')
    
    @classmethod
    def ea(self):
        """
        Shorthand for getting instance of each
        """
        return self.get(self.name=='EACH')
    
    @classmethod
    def lf(self):
        """
        Shorthand for getting instance of linear foot
        """
        return self.get(self.name=='L.F.')
    
    @classmethod
    def lbs(self):
        """
        Shorthand for getting indstancce of pounds
        """
        return self.get(self.name=='LBS.')

    @classmethod
    def pair(self):
        return self.get(self.name=='PAIR')
    
    @classmethod
    def sf(self):
        return self.get(self.name=='S.F.')
    
    @classmethod
    def sy(self):
        return self.get(self.name=='S.Y.')
    
    @classmethod
    def tons(self):
        return self.get(self.name=='TONS')
    
    class Meta:
        db_table = 'unit'


class Company(BaseModel):
    """
    Define table representing the list of contracting companies
    """
    name = CharField()
    description = CharField()
    class Meta:
        db_table = 'company'
        indexes = (
            (("name",), True),
        )


class BidEvent(BaseModel):
    """
    Define table representing a bid opening for a particular project.
    """
    project_number = CharField()
    project_name = CharField()
    bid_open_date = DateTimeField()
    project_zone = IntegerField()
    project_length_ft = FloatField()
    awarded_company = ForeignKeyField(Company, backref='bid_events')

    @staticmethod
    def to_datetime(datetime_str):
        dt = datetime.datetime.strptime(
            datetime_str,
            DATE_FORMAT
        )

        return dt

    @staticmethod
    def to_date_str(dt):
        dt_str = dt.strftime(DATE_FORMAT)
        
        return dt_str

    @classmethod
    def bid_events(self, where_clauses=None):

        combined_where = (ContractorBid.company==BidEvent.awarded_company) 

        if where_clauses:
            combined_where = combined_where & where_clauses
        
        q = (BidEvent.select(
                BidEvent.project_name.alias('project_name'),
                BidEvent.project_number.alias('project_number'),
                fn.strftime(DATE_FORMAT, BidEvent.bid_open_date).alias('bid_open_date'),
                BidEvent.id.alias('id'),
                Company,
                fn.sum(
                    BidItem.quantity*ContractorBid.unit_cost).alias('total_award'),
                    fn.sum(BidItem.quantity*BidItem.engineer_unit_cost).alias('total_engineer'))
                .join(BidItem)
                .join(ContractorBid)
                .switch(BidEvent)
                .join(Company)
                .where(combined_where)
                .group_by(BidEvent)
                .order_by(BidEvent.bid_open_date))
        return q

    class Meta:
        db_table = 'bid_event'
        indexes = (
            (("project_number", "bid_open_date"), True),
        )


class ContractItem(BaseModel):
    """
    Define table representing the list of unique contract items which
    may be used in different bids.

    This relationship between ContractItem and BidItem will allow
    for queries identifying all projects using a particular item.
    """
    name = CharField()
    description = CharField()
    unit = ForeignKeyField(Unit, backref='contract_items')

    class Meta:
        db_table = 'contract_item'
        indexes = (
            (("name", "unit"), True),
        )

class BidItem(BaseModel):
    """
    Define table representing the list of bids for a particular event
    There is ONLY one contract_item per bid_item per bid_event
    """
    contract_item = ForeignKeyField(ContractItem, backref='bid_items')
    bid_event = ForeignKeyField(BidEvent, backref='bid_items')
    quantity = FloatField()
    engineer_unit_cost = FloatField()
    item_number = IntegerField()

    @classmethod
    def bid_items(self, where_clause):
        return (BidItem.select(
            BidItem,
            fn.strftime(DATE_FORMAT, BidEvent.bid_open_date).alias('bid_open_date'))
                       .join(ContractItem)
                       .join(Unit)
                       .switch(BidItem)
                       .join(BidEvent)
                       .switch(ContractItem)
                       .join(UnitConversion, JOIN.LEFT_OUTER)
                       .where(where_clause))
    
    @property
    def awarded_contractor_bid(self):
        return (ContractorBid.select()
                             .join(BidItem)
                             .join(ContractItem)
                             .join(Unit)
                             .where(
                                 (ContractorBid.bid_item==self) &
                                 (ContractorBid.company==self.bid_event.awarded_company)
                             )).get()
    class Meta:
        db_table = 'bid_item'
        indexes = (
            (("contract_item", "bid_event"), True),
        )


class UnitConversion(BaseModel):
    """
    Define unit conversions for a particular bid_item
    """
    contract_item = ForeignKeyField(ContractItem, 
        null=True,
        backref='unit_conversions')
    convert_to_unit = ForeignKeyField(Unit)
    conversion_factor = FloatField()
    short_name = CharField()
    description = CharField()

    class Meta:
        db_table = 'unit_conversion'

class ContractorBid(BaseModel):
    """
    Define a table representing various contractor bids for each
    bid item specified in the BidItem table.
    """
    bid_item = ForeignKeyField(BidItem, backref='contractor_bids')
    company = ForeignKeyField(Company, backref='contractor_bids')
    unit_cost = FloatField()

    @classmethod
    def contractor_bids(self, where_clauses):
        return (self.select(
                Company.id.alias('company_id'),
                Company.name.alias('company'),
                BidItem.id.alias('bid_item_id'),
                BidItem.quantity.alias('quantity'),
                Unit.name.alias('units'),
                ContractItem.name.alias('item_name'),
                ContractorBid.unit_cost.alias('estimate'),
                BidEvent.id.alias('bid_event_id'),
                BidEvent.project_name.alias('project_name'),
                BidEvent.project_number.alias('project_number'),
                fn.strftime(DATE_FORMAT, BidEvent.bid_open_date).alias('bid_open_date'))
            .join(BidItem)
            .join(ContractItem)
            .join(Unit)
            .switch(BidItem)
            .join(BidEvent)
            .switch(ContractorBid)
            .join(Company)
            .where(where_clauses))
    
    class Meta:
        db_table = 'contractor_bid'


MODELS = (
    Unit,
    BidEvent,
    Company,
    ContractItem,
    BidItem,
    ContractorBid,
    UnitConversion)
