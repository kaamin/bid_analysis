LUMP_SUM_ITEM_LIST = ['mobilization', 'water_control', 'traffic_ctrl',
                      'trench_falsework', 'trench', 'stormwater',
                      'non_stormwater', 'dust']

bid_item_queries = {
#=============== LUMP SUM ITEMS =================
    'mobilization':{'title': 'Mobilization',
                 'query': 'MOBILIZATION',
                 'not_contains': None,
                 'units': 'L.S.'},
    
    'water_control':{'title': 'Water Control',
                 'query': 'WATER CONTROL',
                 'not_contains': None,
                 'units': 'L.S.'}, 

    'traffic_ctrl':{'title': 'Traffic Control',
                 'query': 'TRAFFIC CONTROL',
                 'not_contains': None,
                 'units': 'L.S.'},     
    
    'trench_falsework':{'title': 'Trench Safety System and Falsework',
                 'query': 'TRENCH SAFETY SYSTEM AND FALSEWORK',
                 'not_contains': None,
                 'units': 'L.S.'},         

    'trench':{'title': 'Trench Safety System',
                 'query': 'TRENCH SAFETY SYSTEM',
                 'not_contains': ['FALSEWORK'],
                 'units': 'L.S.'},             
    
    'stormwater':{'title': 'Stormwater and Non-Stormwater',
                 'query': 'STORMWATER AND NON-STORMWATER',
                 'not_contains': None,
                 'units': 'L.S.'},                 

    'non_stormwater':{'title': 'Non-Stormwater Discharge',
                 'query': 'NON-STORMWATER DISCHARGE',
                 'not_contains': None,
                 'units': 'L.S.'},                     

    'dust':{'title': 'Dust Abatement',
                 'query': 'DUST ABATEMENT',
                 'not_contains': None,
                 'units': 'L.S.'},      

}