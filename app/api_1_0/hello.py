from . import api

@api.route('/hello', methods=['GET'])
def hello():
    return 'Hello World!'
