from functools import reduce
import operator

from flask import request, jsonify
from peewee import fn, PeeweeException

from . import api
from ..model import (BidEvent, Company, Unit, ContractItem, 
    BidItem, ContractorBid)
from ..exceptions import ValidationError

@api.route('/bid_events/all', methods=['GET'])
def bid_events_all():
    """
    Returns a list of all bid events as a JSON string.
    """

    bid_events = BidEvent.bid_events()

    response = {
        'status': "success",
        'data': {
            'bid-events': list(bid_events.dicts())
        }
    }

    return jsonify(response)

@api.route('/bid_events', methods=['GET'])
def bid_events_filter():
    """
    Return a list of bid events as as JSON string, given query paramters.
    """
    query_parameters = request.args

    id = query_parameters.get('id')
    project_number = query_parameters.get('project_number')
    project_name = query_parameters.get('project_name')

    date_from = query_parameters.get('date_from')
    date_to = query_parameters.get('date_to')

    where_clauses = []

    if id:
        try:
            where_clauses.append(
                BidEvent.id==int(id)
            )
        except ValueError:
            raise ValidationError("Invalid bid_event_id specified. bid_event_id should only be integers")

    if project_name:
        where_clauses.append(
            BidEvent.project_name.contains(project_name)
        )
    
    if project_number:
        where_clauses.append(
            BidEvent.project_number.contains(project_number)
        )

    try:    
        if date_from:
            where_clauses.append(
                BidEvent.bid_open_date>=BidEvent.to_datetime(date_from)
            )
        
        if date_to:
            where_clauses.append(
                BidEvent.bid_open_date<=BidEvent.to_datetime(date_to)
            )
    except ValueError:
        raise ValidationError("Invalid date format used. Please use MM/DD/YYYY.")  
    
    where_clauses = reduce(operator.or_, where_clauses)
    bid_events = BidEvent.bid_events(where_clauses)

    response = {
        'status': "success",
        'data': {
            'bid-events': list(bid_events.dicts())
        }
    }    

    return jsonify(response)
