from functools import reduce
import operator

from flask import request, jsonify
from peewee import PeeweeException

from . import api
from ..model import (BidEvent, Company, Unit, ContractItem, 
    BidItem, ContractorBid)

@api.route('/contract_items/all', methods=['GET'])
def contract_items_all():
    """
    Return a list of contract items as as JSON string, given query paramters.
    """
    response = {}
    response['status'] = ''
    response['data'] = {}

    status_code = 200
    
    try:
        contract_items = (ContractItem.select(ContractItem, 
                                                Unit.name.alias('units'))
                                        .join(Unit))
        response['status'] = 'success'
        response['data']['contract-items'] = list(contract_items.dicts())                                         
    
    except PeeweeException:
        response['status'] = 'error'
        response['data']['message'] = ("A database error occured.")
        status_code = 500
    
    except:
        response['status'] = 'error'
        response['data']['message'] = ("A server error occured.")
        status_code = 500

    return jsonify(response), status_code

@api.route('/contract_items', methods=['GET'])
def contract_items_filter():
    """
    Return a list of bid events as as JSON string, given query paramters.
    """
    query_parameters = request.args

    name = query_parameters.get('name')
    units = query_parameters.get('units')

    where_clauses = []

    is_arguments_provided = (name or units)
   
    response = {}
    response['status'] = ''
    response['data'] = {}

    status_code = 200

    try:
        if name:
            where_clauses.append(ContractItem.name.contains(name))
        
        if units:
            where_clauses.append(Unit.name==units)

        if is_arguments_provided:
            query = (ContractItem.select(ContractItem, 
                                            Unit.name.alias('units'))
                                    .join(Unit)
                                    .where(reduce(operator.and_, where_clauses)))
  
            response['status'] = 'success'
            response['data']['contract-items'] = list(query.dicts())
        else:
            response['status'] = 'fail'
            response['data']['message'] = "No arguments provided."
            status_code = 400

    except PeeweeException:
        response['status'] = 'error'
        response['data']['message'] = ("A database error occured.")
        status_code = 500
    
    except:
        response['status'] = 'error'
        response['data']['message'] = ("A server error occured.")
        status_code = 500

    return jsonify(response)
