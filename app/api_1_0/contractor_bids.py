from functools import reduce
import operator

from flask import request, jsonify
from peewee import PeeweeException

from . import api
from ..model import (BidEvent, Company, Unit, ContractItem, 
    BidItem, ContractorBid)
from ..exceptions import ValidationError

@api.route('/contractor_bids/all', methods=['GET'])
def contractor_bids_all():
    """
    Return a list of ALL contractor as as JSON string.
    """
    contractor_bids = ContractorBid.contractor_bids(where_clauses=None)

    response = {
        'status': "success",
        'data': {
            'contractor-bids': list(contractor_bids.dicts())  
        }
    }

    return jsonify(response), 200

@api.route('/contractor_bids', methods=['GET'])
def contractor_bids_filter():
    """
    Return a list of SELECTED contractor bids as as JSON string, given query paramters.
    """
    query_parameters = request.args

    bid_event_id = query_parameters.get('bid_event_id')
    item = query_parameters.get('item')
    item_not_contains = query_parameters.get('item_name_not_contains')
    project_number = query_parameters.get('project_number')
    project_name = query_parameters.get('project_name')
    company = query_parameters.get('company')

    units = query_parameters.get('units')
    date_from = query_parameters.get('date_from')
    date_to = query_parameters.get('date_to')
    
    where_clauses = []
    
    is_arguments_provided = (bid_event_id or item or project_number or 
        project_name or company or date_from or date_to or item_not_contains)
    
    if not is_arguments_provided:
        raise ValidationError("No arguments provided") 

    if bid_event_id:
        try:
            where_clauses.append(
                BidEvent.id == int(bid_event_id)
            )
        except ValueError:
            raise ValidationError("Invalid bid_event_id specified. bid_event_id should only be integers")

    if item:
        where_clauses.append(
            ContractItem.name.contains(item)
        )

    if units:
        where_clauses.append(
            Unit.name.contains(units)
        )

    if item_not_contains:
        for item_not_cont in item_not_contains.split(';'):
            where_clauses.append(
                ~ContractItem.name.contains(item_not_cont)
            )

    if company:
        where_clauses.append(
            Company.name.contains(company)
        )

    if project_name:
        where_clauses.append(
            BidEvent.project_name.contains(project_name)
        )
    
    if project_number:
        where_clauses.append(
            BidEvent.project_number.contains(project_number)
        )
    
    try:
        if date_from:
            where_clauses.append(
                BidEvent.bid_open_date>=BidEvent.to_datetime(date_from)
            )
        
        if date_to:
            where_clauses.append(
                BidEvent.bid_open_date<=BidEvent.to_datetime(date_to)
            )
    except ValueError:
        raise ValidationError("Invalid date format used. Please use MM/DD/YYYY.")
    
    where_clauses = reduce(operator.and_, where_clauses)
    contractor_bids = ContractorBid.contractor_bids(where_clauses)

    response = {
        'status': "success",
        'data': {
            'contractor-bids': list(contractor_bids.dicts())  
        }
    }

    return jsonify(response), 200
