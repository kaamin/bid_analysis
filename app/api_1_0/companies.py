from functools import reduce
import operator

from flask import request, jsonify

from . import api
from ..model import (BidEvent, Company, Unit, ContractItem, 
    BidItem, ContractorBid)

@api.route('/companies/all', methods=['GET'])
def companies_all():
    """
    Return a list of contract items as as JSON string, given query paramters.
    """
    companies = Company.select(Company)

    return jsonify(list(companies.dicts()))

@api.route('/companies', methods=['GET'])
def companies_filter():
    """
    Return a list of bid events as as JSON string, given query paramters.
    """
    query_parameters = request.args

    name = query_parameters.get('name')

    result = []

    if name:
        query = (Company.select(Company).where(Company.name.contains(name)))
        result = list(query.dicts())
                        
    return jsonify(result)