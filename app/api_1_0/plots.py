from functools import reduce
import io
import operator

from flask import request, jsonify, send_file
from peewee import fn

# import numpy as np
# from scipy import stats

# import matplotlib.pyplot as plt
# import matplotlib.ticker as ticker
# plt.style.use('ggplot')

from . import api
from ..model import (BidEvent, Company, Unit, ContractItem, 
    BidItem, ContractorBid)

# def generate_bid_item_ridge_plot(bid_item_name, qty, units, bids, win):
#     """
#     Generate ridge plots
#     """
    
#     #Create distribution
#     pdf = stats.gaussian_kde(bids)
#     x = np.linspace(0, max(bids), 50)
#     y = pdf(x)
    
#     x_min = min(bids)
#     x_max = max(bids)
#     y_max = max(y)
    
#     fig, ax = plt.subplots(1, 1)
    
#     fig.set_size_inches(4, 1)
    
#     x_tick_locations = sorted([x_min, x_max])
    
#     ax.plot(x, y, lw=2)  
    
#     ax.plot([win, win], [0, y_max], lw=2)
    
#     ax.yaxis.set_visible(False)
#     ax.grid(False)
#     ax2 = ax.twiny()
#     ax2.grid(False)
#     ax2.set_xlim(ax.get_xlim())
#     ax2.xaxis.set_major_locator(ticker.FixedLocator([win,]))
#     ax.xaxis.set_major_locator(ticker.FixedLocator(x_tick_locations))

#     #from https://towardsdatascience.com/python-plotting-api-expose-your-scientific-python-plots-through-a-flask-api-31ec7555c4a8
#     bytes_image = io.BytesIO()
#     fig.savefig(bytes_image, bbox_inches = "tight")
#     bytes_image.seek(0)

#     return bytes_image


# @api.route('/ridge_plot', methods=['GET'])
# def bid_item_ridge_plot():
#     """
#     """

#     query_parameters = request.args

#     bid_event_id = query_parameters.get('bid_event_id')
#     contract_item_id = query_parameters.get('contract_item_id')

#     if bid_event_id:

#         bid_item = (BidItem.select(BidItem, 
#                         ContractItem, 
#                         Unit)
#                 .join(ContractItem)
#                 .join(Unit)
#                 .switch(BidItem)
#                 .join(BidEvent)
#                 .where((BidEvent.id==bid_event_id) & (ContractItem.id==contract_item_id))
#                 .get())
        
#         contractor_bids = np.array(
#             [cb.unit_cost for cb in bid_item.contractor_bids]
#         )

#         win = bid_item.winning_bid

#         bytes_obj = generate_bid_item_ridge_plot(
#             bid_item_name=bid_item.contract_item.name,
#             qty=bid_item.quantity,
#             units=bid_item.contract_item.unit.name,
#             bids=contractor_bids,
#             win=win
#         )

#     return send_file(bytes_obj,
#                      attachment_filename='plot.png',
#                      mimetype='image/png' )








