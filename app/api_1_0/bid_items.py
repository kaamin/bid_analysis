import datetime
from functools import reduce
import operator
import os
import re

from flask import request, jsonify, session
from peewee import fn, SQL, Value

import pandas as pd
import numpy as np

from . import api
from ..model import (BidEvent, Company, Unit, ContractItem, 
    BidItem, ContractorBid, UnitConversion)

from ..data import bid_item_queries, LUMP_SUM_ITEM_LIST
from ..exceptions import ValidationError

BASE_PATH = api.root_path


@api.route('/bid_items', methods=['GET'])
def bid_items_filter():
    """
    Return a list of bid events as as JSON string, given query paramters.
    """
    query_parameters = request.args

    bid_event_id = query_parameters.get('bid_event_id')
    id = query_parameters.get('id')
    name = query_parameters.get('name')
    item = query_parameters.get('item')
    item_not_contains = query_parameters.get('item_name_not_contains')

    units = query_parameters.get('units')
    date_from = query_parameters.get('date_from')
    date_to = query_parameters.get('date_to')

    is_arguments_provided = (bid_event_id or id or name or item or date_from or date_to)

    if not is_arguments_provided:
        raise ValidationError("No arguments provided") 
    
    where_clauses = []

    if bid_event_id:
        try:
            where_clauses.append(
                BidEvent.id == int(bid_event_id)
            )
        except ValueError:
            raise ValidationError("Invalid bid_event_id specified. bid_event_id should only be integers")
    
    if id:
        try:
            where_clauses.append(
                BidItem.id == int(id)
            )
        except:
            raise ValidationError("Invalid id specified. id should only be integers")

    if item:
        where_clauses.append(
            ContractItem.name.contains(item)
        )

    if units:
        where_clauses.append(
            Unit.name.contains(units)
        )

    if item_not_contains:
        for item_not_cont in item_not_contains.split(';'):
            where_clauses.append(
                ~ContractItem.name.contains(item_not_cont)
            )

    if name:
        where_clauses.append(
                ContractItem.name.contains(name)
        )
        
    try:
        if date_from:
            where_clauses.append(
                BidEvent.bid_open_date>=BidEvent.to_datetime(date_from)
            )
        
        if date_to:
            where_clauses.append(
                BidEvent.bid_open_date<=BidEvent.to_datetime(date_to)
            )
    except ValueError:
        raise ValidationError("Invalid date format used. Please use MM/DD/YYYY.")

    where_clauses_combined = reduce(operator.and_, where_clauses)

    query = BidItem.bid_items(where_clauses_combined)
    
    result = [
        {
            'id': bid_item.id,
            'bid_event_id': bid_item.bid_event.id, 
            'item_number': bid_item.item_number,
            'item_name': bid_item.contract_item.name,
            'quantity': bid_item.quantity,
            'bid_open_date': bid_item.bid_open_date,
            'units': bid_item.contract_item.unit.name,
            'engineer_unit_cost': bid_item.engineer_unit_cost,
            'awarded_unit_cost': bid_item.awarded_contractor_bid.unit_cost,
            'awarded_company_name': bid_item.awarded_contractor_bid.company.name,
            'median_unit_cost': np.median([cb.unit_cost for cb in bid_item.contractor_bids])
        } 
        for bid_item in query
    ]

    response = {
        'status': "success",
        'data': {
            'bid-items': result  
        }
    }     
    return jsonify(response), 200

def query(contract_item_filter, exclude_filters, unit_filter):
    """
    Generates a query result based on the selected filters.
    """
    where_clauses = []

    where_clauses.append(
        ContractItem.name.contains(contract_item_filter)
    )

    where_clauses.append(
         Unit.name==unit_filter
    )

    if exclude_filters:
        for exclude_filter in exclude_filters:
            where_clauses.append(
                ~(ContractItem.name.contains(exclude_filter)) 
            )

    bid_items = (BidItem.select(BidItem, 
                            ContractItem, 
                            Unit)
                    .join(ContractItem)
                    .join(Unit)
                    .order_by(BidItem.quantity)
                    .where(reduce(operator.and_, where_clauses)))    
    return bid_items

def project_bid_items_all(bid_event_id, company_id):
    bid_items = (ContractorBid.select(
                                   Company.name.alias('name'),
                                   fn.sum(BidItem.quantity * ContractorBid.unit_cost).alias('total_bid'))
                              .join(BidItem)
                              .join(BidEvent)
                              .switch(ContractorBid)
                              .join(Company)
                              .group_by(Company)
                              .where((BidEvent.id==bid_event_id) & (Company.id==company_id))
                              .order_by(SQL('total_bid'))
                              .get())    
    return bid_items

@api.route('/bid_items_by_group', methods=['GET'])
def bid_items_by_group_filter():
    """
    Return a list of bid events as as JSON string, given query paramters.
    """
    query_parameters = request.args

    group_name = query_parameters.get('group_name')

    result = {}

    if group_name and group_name in bid_item_queries:
        contract_item_filter = bid_item_queries[group_name]['query']
        exclude_filters = bid_item_queries[group_name]['not_contains']
        unit_filter = bid_item_queries[group_name]['units']

        companies = []
        unit_costs = []
        bid_open_dates = []
        qtys = []
        project_names = []
        project_totals = []

        bid_items = query(contract_item_filter, exclude_filters, unit_filter)

        for bid_item in bid_items:
            for contractor_bid in bid_item.contractor_bids:
                contractor_bid_summary = project_bid_items_all(
                    bid_item.bid_event.id, contractor_bid.company.id)
                
                companies.append(contractor_bid.company.name)
                unit_costs.append(contractor_bid.unit_cost)
                bid_open_dates.append(bid_item.bid_event.bid_open_date)
                qtys.append(bid_item.quantity)
                project_names.append(bid_item.bid_event.project_name)
                project_totals.append(contractor_bid_summary.total_bid)

        result = {
            'company': companies,
            'unit_cost': unit_costs,
            'bid_open_date': bid_open_dates,
            'qty': qtys,
            'project_name': project_names,
            'project_total': project_totals,
        }
        
    else:
        result = {
            'message': 'group name not specified or does not exist,'
        }
    return jsonify(result) 

@api.route('/bid_items_statistics', methods=['GET', 'OPTIONS'])
def bid_items_statistics():
    """
    Return a list of bid events as as JSON string, given query paramters.
    """
    query_parameters = request.args

    group_name = query_parameters.get('group_name')

    result = []

    if group_name and group_name in bid_item_queries:
        filename = os.path.join(BASE_PATH, f'../data/csv/{group_name}.csv')

        df = pd.read_csv(filename)
        result = {
            'x': df.x.values.tolist(),
            'y_fit': df.y_fit.values.tolist(),
            'conf_hi': df.conf_hi.values.tolist(),
            'conf_lo': df.conf_lo.values.tolist()
        }  
        
    else:
        result = {
            'message': 'group name not specified or does not exist,'
        }

    return jsonify(result) 

@api.route('bid_item_table', methods=['GET', 'POST'])
def bid_item_table():

    query_parameters = request.args

    contract_item_filter = query_parameters.get('search[value]')
    column_order = int(query_parameters.get('order[0][column]'))
    column_order_dir = query_parameters.get('order[0][dir]')
    units_column_filter = query_parameters.get('columns[2][search][value]')

    session['current_search'] = contract_item_filter
    session['current_unit_filter'] = units_column_filter

    print(contract_item_filter, column_order, units_column_filter) 
    
    draw = query_parameters.get('draw')
    
    data_results = []

    where_clauses = ContractItem.name.contains(contract_item_filter)

    #TODO: These values should be retrieved from the database
    valid_units = ['ACRE', 'C.Y.',
                   'EACH', 'GAL', 'L.S.',
                   'L.F.', 'LBS.','PAIR',
                   'S.F.', 'S.Y.', 'TONS']
    
    user_has_selected_units = units_column_filter in valid_units

    if user_has_selected_units in valid_units:
        selected_units = Unit.get(Unit.name==units_column_filter)
        
        where_clauses = where_clauses & \
            ((Unit==selected_units) | (UnitConversion.convert_to_unit==selected_units))

    if contract_item_filter.strip() != '':
        bid_items = BidItem.bid_items(where_clauses)

        for bid_item in bid_items:
            contractor_bids = np.array(
                [cb.unit_cost for cb in bid_item.contractor_bids])

            project_name = bid_item.bid_event.project_name
            bid_open_date = bid_item.bid_event.bid_open_date.strftime("%m/%d/%Y")
            bid_event_id = bid_item.bid_event.id

            contract_item_name = bid_item.contract_item.name
            bid_item_quantity = bid_item.quantity
            unit_name = bid_item.contract_item.unit.name

            engineer_unit_cost = bid_item.engineer_unit_cost
            median_bid = np.median(contractor_bids)
            awarded_contractor_bid = bid_item.awarded_contractor_bid.unit_cost

            if user_has_selected_units:
                if unit_name == units_column_filter:
                    data_results.append([
                        contract_item_name,
                        bid_item_quantity,
                        unit_name,
                        f'${engineer_unit_cost:,.2f}',
                        f'${median_bid:,.2f}',
                        f'${awarded_contractor_bid:,.2f}',
                        project_name,
                        bid_open_date,
                        bid_event_id,
                        ''
                    ])     

                for unit_conversion in bid_item.contract_item.unit_conversions:
                    conversion_factor = unit_conversion.conversion_factor
                    converted_to_units = f'{unit_conversion.convert_to_unit.name}'
                    conversion_description = unit_conversion.description

                    converted_quantity = bid_item_quantity * conversion_factor
                    converted_engineer_unit_cost = engineer_unit_cost / conversion_factor
                    converted_median_bid = median_bid / conversion_factor
                    converted_award_bid = awarded_contractor_bid / conversion_factor

                    if converted_to_units == units_column_filter:
                        data_results.append([
                            contract_item_name,
                            f'{converted_quantity:.2f}**',
                            f'{converted_to_units}**',
                            f'${converted_engineer_unit_cost:,.2f}**',
                            f'${converted_median_bid:,.2f}**',
                            f'${converted_award_bid:,.2f}**',
                            project_name,
                            bid_open_date,
                            bid_event_id,
                            conversion_description
                        ])
            else:
                data_results.append([
                    contract_item_name,
                    bid_item_quantity,
                    unit_name,
                    "${:,.2f}".format(engineer_unit_cost),
                    "${:,.2f}".format(median_bid),
                    "${:,.2f}".format(awarded_contractor_bid),
                    project_name,
                    bid_open_date,
                    bid_event_id,
                    ''
                ])

    NUMERIC_COLUMNS = [1, 3, 4, 5]
    DATE_COLUMNS = [7,]

    if column_order in NUMERIC_COLUMNS:
        sort_key = lambda x: float(
            re.sub(
                r'[\$,*]',
                '', 
                str(x[column_order]) ) )        
        
    elif column_order in DATE_COLUMNS:
            sort_key = lambda x: datetime.datetime.strptime(x[column_order], "%m/%d/%Y") 

    else:
        sort_key = lambda x: x[column_order]

    data_results.sort(
        key=sort_key, 
        reverse=(column_order_dir == 'desc'))

    result = {
        "draw": draw,
        "recordsTotal": len(data_results),
        "recordsFiltered": len(data_results),
        "data": data_results,
    }

    return jsonify(result)
