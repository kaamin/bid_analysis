from flask import jsonify
from peewee import PeeweeException

from . import api
from ..exceptions import ValidationError

def bad_request(message, status, status_code):
    response = {
        'status': status,
        'data': {
            'message': message
        }
    }

    return jsonify(response), status_code

@api.errorhandler(ValidationError)
def validation_error(e):
	return bad_request(e.args[0], 'fail', 400)

@api.errorhandler(PeeweeException)
def database_error(e):
    return bad_request("A database error occured.", 'error', 500)

@api.errorhandler(Exception)
def unknown_error(e):
    return bad_request("A server error occured.", 'error', 500)