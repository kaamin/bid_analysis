import csv
import datetime
import logging
import os
import unittest

import peewee
from . import model

DATE_FORMAT = "%m/%d/%Y %H:%M"


DATA_CSV = 'data/import_validated.csv'

def load_csv(csv_filename):
    """
    Loads a csv file into an array of dictionaries, 
    where the keys are the column names.

    Parameters
    ----------
    csv_filename: string
        csv file path.

    Returns:
        list: csv data as a list
    """
    data = []

    with open(csv_filename, mode = 'r') as csv_file:
        csv_reader = csv.DictReader(csv_file)  
        for row in csv_reader:
            data.append(row)  
    return data

def create_tables():
    """
    Creates database tables.
    """
    model.db.create_tables(model.MODELS)
    model.Unit.insert_initial_data()

def import_company(data):
    """
    Imports names of contractor companies from data into the Company database table.

    Parameters
    ----------
    data: list
        Data to import as a list of dictionaries, 
        where keys correspond to column names.
     
    """
    print("Importing Companies...")

    with model.db.atomic():
        for row in data:
            company_name = row['name'].strip()
            description = row['description'].strip()

            #print(company_name)
            
            try:
                model.Company.create(
                    name=company_name,
                    description=description
                )
            except peewee.IntegrityError:
                pass
            
def import_bid_event(data):
    """
    Import bid_event information from data into the Bid Event database table.
    
    Parameters
    ----------
    data: list
        Data to import as a list of dictionaries, 
        where keys correspond to column names.
    """
    
    print("Importing Bid Events...")

    with model.db.atomic():
        for row in data:
            project_name = row['project_name'].strip()
            project_number = row['project_number'].strip()
            
            bid_open_date_str =  row['bid_open_date'].strip()

            project_zone = int(row['project_zone'].strip())
            project_length_ft = float(row['project_length_ft'].strip())

            awarded_company_name = row['awarded_company'].strip()

            awarded_company = model.Company.get(
                model.Company.name==awarded_company_name)
            
            bid_open_date = datetime.datetime.strptime(
                bid_open_date_str,
                DATE_FORMAT)

            try:
                model.BidEvent.create(
                    project_number=project_number,
                    project_name=project_name,
                    bid_open_date=bid_open_date,
                    project_zone=project_zone,
                    project_length_ft=project_length_ft,
                    awarded_company=awarded_company
                )
                #print(bid_open_date_str, ": ", project_name)
            except peewee.IntegrityError as peewee_error:
                print(peewee_error)

def import_contract_item(data):
    """
    Import Contract Item information from data into the Contract Item database table.
    
    Parameters
    ----------
    data: list
        Data to import as a list of dictionaries, 
        where keys correspond to column names.
    """    
    print("Import Contract Items...")

    with model.db.atomic():
        for row in data:
            unit_name = row['units'].strip()
            unit = model.Unit.get(
                model.Unit.name==unit_name)

            contact_item_name = row['name'].strip()
            description = row['description'].strip()
            
            try:
                model.ContractItem.create(
                    name=contact_item_name,
                    description=description,
                    unit=unit
                )
                #print(contact_item_name)
            except peewee.IntegrityError:
                pass

def import_bid_item(data):
    """
    Import Bid Item information from data into the Bid Item database table.

    Note: Bid Item is referenced to Bid Event, Company, and Contract Item.
    Therefore, these tables must already be created and assocated data already
    inserted prior to inserting data into Bid Item.
    
    Parameters
    ----------
    data: list
        Data to import as a list of dictionaries, 
        where keys correspond to column names.    
    """
    print("Import Bid Items...")

    with model.db.atomic():
        for row in data:
            contract_item_name = row['contract_item_name'].strip()
            unit_name = row['units'].strip()
            unit = model.Unit.get(model.Unit.name==unit_name)

            where_clause = ((model.ContractItem.name==contract_item_name) &
                            (model.ContractItem.unit==unit))
            
            contract_item = model.ContractItem.get(where_clause)

            bid_open_date_str = row['bid_open_date'].strip()
            bid_open_date=datetime.datetime.strptime(
                bid_open_date_str,
                DATE_FORMAT)

            project_number = row['project_number'].strip()
            item_number = int(row['item_number'].strip())
            
            bid_event = model.BidEvent.get(
                (model.BidEvent.project_number==project_number) &
                (model.BidEvent.bid_open_date==bid_open_date)
            )
            
            quantity = float(row['quantity'].strip())
            
            engineer_unit_cost = float(row['engineer_unit_cost'].strip()) 
            
            try:
                model.BidItem.create(
                    contract_item=contract_item,
                    bid_event=bid_event,
                    quantity=quantity,
                    engineer_unit_cost=engineer_unit_cost,
                    item_number=item_number
                )  

                #print(item_number, contract_item_name)

            except peewee.IntegrityError:
                pass

def import_contractor_bid(data):
    """
    Contractor Bid Item information from data into the Contractor Bid database table.

    Note: Bid Item is referenced to Bid Item and Company.
    Therefore, these tables must already be created and assocated data already
    inserted prior to inserting data into Contractor Bid.
    
    Parameters
    ----------
    data: list
        Data to import as a list of dictionaries, 
        where keys correspond to column names.    
    """    
    print("Import Contractor Bids...")

    with model.db.atomic():
        for row in data:
            project_number = row['project_number'].strip()
            company_name = row['company_name'].strip()

            bid_open_date_str = row['bid_open_date'].strip()
            bid_open_date=datetime.datetime.strptime(
                        bid_open_date_str,
                        DATE_FORMAT)        
            
            unit = model.Unit.get(model.Unit.name==row['units'].strip())
            contract_item_name = row['contract_item_name'].strip()

            where_clause = ((model.ContractItem.name==contract_item_name) &
                            (model.ContractItem.unit==unit))
            contract_item = (model.ContractItem.select()
                                               .join(model.Unit)
                                               .where(where_clause)
                                               .get())            

            bid_event = model.BidEvent.get(
                (model.BidEvent.project_number == project_number) &
                (model.BidEvent.bid_open_date == bid_open_date)
            )

            bid_item = model.BidItem.get(
                (model.BidItem.contract_item == contract_item) &
                (model.BidItem.bid_event == bid_event)
            )

            company = model.Company.get(
                model.Company.name==company_name
            )

            
            unit_cost = float(row['unit_cost'].strip()) 
            
            try:
                model.ContractorBid.create(
                    bid_item=bid_item,
                    company=company,
                    unit_cost=unit_cost
                )

                #print(bid_item.id, contract_item.name)
            except peewee.IntegrityError:
                pass   

def import_unit_conversion(data):
    """
    Import Contract Item information from data into the Contract Item database table.
    
    Parameters
    ----------
    data: list
        Data to import as a list of dictionaries, 
        where keys correspond to column names.
    """    
    print("Import Unit Conversion Items...")

    with model.db.atomic():
        for row in data:
            contract_item_name = row['contract_item'].strip()

            unit_name = row['units'].strip()
            unit = model.Unit.get(
                model.Unit.name==unit_name)

            convert_to_unit_name = row['convert_to_unit'].strip()
            convert_to_unit = model.Unit.get(
                model.Unit.name==convert_to_unit_name)
            
            where_clause = ((model.ContractItem.name==contract_item_name) &
                            (model.ContractItem.unit==unit))
            
            contract_item = (model.ContractItem.select()
                                               .join(model.Unit)
                                               .where(where_clause)
                                               .get())      
            
            conversion_factor = float(row['conversion_factor'].strip())
            short_name = row['short_name'].strip()
            description = row['description'].strip()
            
            try:
                model.UnitConversion.create(
                    contract_item=contract_item,
                    convert_to_unit=convert_to_unit,
                    conversion_factor=conversion_factor,
                    short_name=short_name,
                    description=description,
                )

            except peewee.IntegrityError:
                pass

if __name__ == '__main__':
    data = load_csv(DATA_CSV)

    create_tables()
    import_company(data)
    import_bid_event(data)
    import_contract_item(data)
    import_bid_item(data)
    import_contractor_bid(data)    
