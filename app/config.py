"""
Configuration Definitions
"""
import os

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

class BaseConfig:
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'hard to guess string'

    @staticmethod
    def init_app(app):
        pass

class DevelopmentConfig(BaseConfig):
    DEBUG = True
    DATABASE_PATH = os.path.join(BASE_DIR, 'db/bid_data.db')

class TestingConfig(BaseConfig):
    TESTING = True
    DATABASE_PATH = os.path.join(BASE_DIR, 'db/test_case.db')

config = {
    'testing': TestingConfig,
    'development': DevelopmentConfig,
    'default': DevelopmentConfig
}