import os

from flask import Flask

from .config import config
from peewee import Proxy, SqliteDatabase
from flask_bootstrap import Bootstrap

db = Proxy()
bootstrap = Bootstrap()

def create_app(config_name):
    #create and configure the app

    #Define sqlite database. 
    #Note that 'foreign_keys' parameter is specified,
    #in order for enforce relationships
    sqlite_db = SqliteDatabase(
        config[config_name].DATABASE_PATH,
        pragmas={'foreign_keys': 1})

    db.initialize(sqlite_db)

    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(config[config_name])

    bootstrap.init_app(app)

    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from .api_1_0 import api as api_1_0_blueprint
    app.register_blueprint(api_1_0_blueprint, url_prefix='/api/v1.0')
    
    return app