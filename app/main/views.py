from functools import reduce
import operator
import os
import urllib.parse

from bokeh.embed import components
from bokeh.resources import INLINE
from bokeh.util.string import encode_utf8

from bokeh.plotting import figure, show
from bokeh.io import output_notebook, output_file
from bokeh.models import HoverTool, Select, DatetimeTickFormatter, ColumnDataSource, Label, Band, AjaxDataSource
from bokeh.application.handlers import FunctionHandler
from bokeh.application import Application
from bokeh.layouts import row, widgetbox, column
from bokeh.models.widgets import DataTable, DateFormatter, TableColumn
from bokeh.transform import jitter
from bokeh.models.glyphs import Line, Text

from flask import request, jsonify, render_template, url_for, abort, session
from peewee import fn

import numpy as np
import pandas as pd
from scipy import stats

from . import main
from .. import db
from ..model import (BidEvent, Company, Unit, ContractItem, 
    BidItem, ContractorBid)
from ..data import bid_item_queries

BASE_PATH = main.root_path

@main.before_request
def _database_connnect():
    if db.is_closed():
        db.connect()

@main.teardown_request
def _database_close(exc):
    if not db.is_closed():
        db.close()

@main.route('/')
def index():
    return render_template('index.html')

@main.route('/bid-event/<int:bid_event_id>', methods=['GET'])
def bid_event_summary(bid_event_id):

    bid_event = BidEvent.get(BidEvent.id==bid_event_id)

    bid_items = (BidItem.select(BidItem, 
                            ContractItem, 
                            Unit)
                    .join(ContractItem)
                    .join(Unit)
                    .switch(BidItem)
                    .join(BidEvent)
                    .where(BidEvent.id==bid_event_id))
    
    data_results = []

    for bid_item in bid_items:
        item_number = bid_item.item_number
        contract_item_name = bid_item.contract_item.name
        quantity = bid_item.quantity
        unit_name = bid_item.contract_item.unit.name
        
        contractor_bids = np.array(
            [cb.unit_cost for cb in bid_item.contractor_bids])

        awarded_contractor_bid = bid_item.awarded_contractor_bid.unit_cost
        engineer_unit_cost = bid_item.engineer_unit_cost
        median_bid = np.median(contractor_bids)
        min_bid = min(contractor_bids)
        max_bid = max(contractor_bids)

        data_results.append( {
            'id': bid_item.id,
            'item_number': item_number,
            'contract_item_name': contract_item_name,
            'quantity': quantity,
            'unit_name': unit_name, 
            'engineer_unit_cost': f'${engineer_unit_cost:.2f}',
            'awarded_contractor_bid': awarded_contractor_bid,
            'median_bid': median_bid,
            'min_bid': min_bid,
            'max_bid': max_bid
        })

    return render_template(
        "contract-item-list.html", 
        bid_event=bid_event, 
        bid_items=data_results)

@main.route('/project-list')
def bid_event_project_list():

    bid_events = BidEvent.bid_events()

    return render_template("bid-event-list.html", bid_events=bid_events)

@main.route('/lump-sum-chart/<group_name>', methods=['GET'])
def lump_sum_chart(group_name):
    
    if group_name not in bid_item_queries:
        abort(404)

    current_bid_item = group_name

    filename = os.path.join(BASE_PATH, f'../data/csv/regression_results.csv')

    regression_df = pd.read_csv(filename)

    sel_df = regression_df[regression_df.bid_item==current_bid_item]

    src1 = AjaxDataSource(
        data = {
            'company': [],
            'unit_cost': [],
            'bid_open_date': [],
            'qty': [],
            'project_name': [],
            'project_total': []        
        },   
        data_url=url_for('api.bid_items_by_group_filter', group_name=current_bid_item),
        method='GET',
        polling_interval=65000,
        mode='replace'
    )

    src2 = AjaxDataSource(
        data = {
            'x': [],
            'y_fit': [],
            'conf_lo': [],
            'conf_hi': []
        },    
        data_url=url_for('api.bid_items_statistics', group_name=current_bid_item),
        method='GET',
        polling_interval=65000,
        mode='replace'
    )

    TOOLS = ['save', 'zoom_in','zoom_out', 'pan', 'tap']
    p = figure(plot_width=800, tools=TOOLS)

    units = bid_item_queries[current_bid_item]['units']

    if  units == 'L.S.':
        scatter_x = 'project_total'
    else:
        scatter_x = 'qty'
    p.circle(source=src1, x=scatter_x, y='unit_cost', size=8, fill_alpha=0.10, line_alpha=0.1)
    p.line(source=src2, x='x', y='y_fit', line_width=2, line_alpha=0.8, color="orange")

    band = Band(base='x', lower='conf_lo', upper='conf_hi', source=src2, 
        level='underlay', fill_alpha=0.15, fill_color='blue', line_width=0)
    p.add_layout(band)

    lbl = Label(x=360, y=500, y_units='screen', x_units='screen',
                text="slope={:.3E}, intercept={:6.3f}".format(
                    sel_df.slope.values[0], 
                    sel_df.intercept.values[0]))

    lbl_p = Label(x=360, y=475, y_units='screen', x_units='screen',
                    text="p_slope={:6.3f}, p_intercept={:6.3f}".format(
                        sel_df.p_slope.values[0], 
                        sel_df.p_intercept.values[0]))
    p.add_layout(lbl)
    p.add_layout(lbl_p)
    hover = HoverTool(
        tooltips=[
            ('Project Name', '@project_name'),
            ('Company', '@company'),
            ('Bid', '$@unit_cost{0, 0.00}'),
            ('Total', '$@project_total{0, 0.00}')
        ]
    )
    p.add_tools(hover)

    p.yaxis.axis_label = 'Bid Cost ($)'
     
    if units == 'L.S.':
        p.xaxis.axis_label = 'Total Bid ($)'
    else:
        p.xaxis.axis_label = f'Quantity ({units})'

    js_resources = INLINE.render_js()
    css_resources = INLINE.render_css()
    script, div = components(p, INLINE)

    return render_template(
        "lump-sum-chart.html",
        bid_item_queries=bid_item_queries, 
        title=bid_item_queries[current_bid_item]['title'],
        plot_script=encode_utf8(script), 
        plot_div=encode_utf8(div),
        js_resources=encode_utf8(js_resources),
        css_resources=(css_resources))


@main.route('/bid-item-search', methods=['GET'])
def bid_item_search():
    initial_search = ''
    initial_column_filter = 'ALL'

    if 'current_search' in session:
        initial_search = session['current_search']

    if 'current_unit_filter' in session:
        if session['current_unit_filter'] != '':
            initial_column_filter = session['current_unit_filter']
                                
        
    return render_template(
        "bid-item-search.html",
        initial_search=initial_search,
        initial_column_filter=initial_column_filter)    

@main.route('/bid-item-chart/<int:bid_item_id>', methods=['GET'])
def bid_item_chart(bid_item_id):
    NUM_OF_PTS = 25
    MIN_Y_THRESHOLD = 0.01

    bid_item = (BidItem.select(BidItem, 
                            ContractItem, 
                            Unit)
                    .join(ContractItem)
                    .join(Unit)
                    .switch(BidItem)
                    .join(BidEvent)
                    .where(BidItem.id==bid_item_id)).get()
    
    contract_item_name = bid_item.contract_item.name
    
    contractor_bids = np.array(
        [cb.unit_cost for cb in bid_item.contractor_bids])

    awarded_contractor_bid = bid_item.awarded_contractor_bid.unit_cost
    engineer_unit_cost = bid_item.engineer_unit_cost
    median_bid = np.median(contractor_bids)

    x_min = min(contractor_bids)
    x_max = max(contractor_bids)    

    if contract_item_name == "EXTRA DIRECTED WORK":
        award_data = []
        bid_item_prob_dist = []
    else:
        x = np.linspace(0, x_max, NUM_OF_PTS)
        pdf = stats.gaussian_kde(contractor_bids)
        y = pdf(x)

        idx = np.abs(x - awarded_contractor_bid).argmin()
        
        y_mask = [1.1 * max(y) if i == idx else None for i in range(len(x))]

        award_data = [
            {'x': awarded_contractor_bid, 'y': 0},
            {'x': awarded_contractor_bid + 0.001*x_max, 'y': 1.1 * max(y)}]
        

        eng_data = [
            {'x': engineer_unit_cost, 'y': 0},
            {'x': engineer_unit_cost + 0.001*x_max, 'y': 1.1 * max(y)}]

        bid_item_prob_dist = []
        for i in range(len(x)):
            bid_item_prob_dist.append({'x': x[i], 'y': y[i]})

    result = {
        'series':[
            {
                'name': 'series-1',
                'data': bid_item_prob_dist
            },
            {
                'name': 'series-2',
                'data': award_data},
            {
                'name': 'series-3',
                'data': eng_data}]
        }    
    return jsonify(result)  
