from flask import request, jsonify, render_template, url_for, abort

from . import main

@main.errorhandler(404)
def resource_not_found(error=None):

    return render_template(
        "404.html"), 404   
