## API Definitions

### GET/bid_events/all 
Returns a list of all bid events as a JSON string.
```javascript
[{
    'id': 1,
    'project_number': '1-0-0001-01',
    'project_name': 'Project Name',
    'bid_open_date': 'MM/DD/YYYY',
    'uri': 'url path for further requests'
},]
```

### GET/bid_event?id=<bid_event_id>
Returns data associated with the specified bid event id.
```javascript
{
    'id': 1,
    'project_number': '1-0-0001-01',
    'project_name': 'Project Name',
    'uri': 'url path for further requests'
}
```
### GET/bid_items/all
Retreive summary information for all bid items in all projects

#### Successful Response
**Code:** `200 OK`

**Example Content:**
```javascript
{
  "data": {
    "bid-items": [
      {
        "awarded_company_name": "MAMCO, INC. dba ALABBASI", 
        "awarded_unit_cost": 5.6, 
        "bid_open_date": "01/07/2014", 
        "engineer_unit_cost": 12.0, 
        "id": 6, 
        "item_name": "EXCAVATION", 
        "item_number": 6, 
        "median_unit_cost": 11.0, 
        "quantity": 6176.0, 
        "units": "C.Y."
      }, 
      {
        "awarded_company_name": "MAMCO, INC. dba ALABBASI", 
        "awarded_unit_cost": 31.0, 
        "bid_open_date": "01/07/2014", 
        "engineer_unit_cost": 20.0, 
        "id": 7, 
        "item_name": "ROADWAY EXCAVATION", 
        "item_number": 7, 
        "median_unit_cost": 25.0, 
        "quantity": 4769.0, 
        "units": "C.Y."
      }, 

      ...
     ]
    }
}
```

### GET/bid_items?
Retrieve filtered contractor bid estimates.

#### URL Parameters

| Parameter Name       | Type   | Description                                                            |
|----------------------|--------|------------------------------------------------------------------------|
|bid_event_id          | integer| id numner for a specific project                                       |
|item                  | string | text to find within bid item name                                      |
|item_name_not_contains| string | list of semi-colon text to exclude from bid item name.                 |
|units                 | string | Units, such as L.S., C.Y.                                              |
|date_from             | string | Include only results after this date. Must be formated as: MM/DD/YYYY  |
|date_to               | string | Include results before this date. Must be formated as: MM/DD/YYYY      |

#### Error: Invalid Date
If the provided date was not formatted as "MM/DD/YYYY" then the following error will occur

**Code:** `400 BAD REQUEST`

**Content:**
```javascript
{
  "data": {
    "message": "Invalid date format used. Please use MM/DD/YYYY."
  }, 
  "status": "fail"
}
```

#### Error: No arguments provided
At least one of the parameters above must be provided in order to retrieve a successful response.

**Code:** `400 BAD REQUEST`

**Content:**
```javascript
{
  "data": {
    "message": "No arguments provided"
  }, 
  "status": "fail"
}
```


#### Successful Response

**Code:** `200 OK`

**Example API call:** `GET bid_items?item=excavation&item_name_not_contains=trench;basin&date_from=01/01/2019`

**Example Content:**
```javascript
{
  "data": {
    "bid-items": [
      {
        "awarded_company_name": "MAMCO, INC. dba ALABBASI", 
        "awarded_unit_cost": 13.0, 
        "bid_open_date": "04/03/2019", 
        "engineer_unit_cost": 12.0, 
        "id": 1172, 
        "item_name": "CHANNEL EXCAVATION", 
        "item_number": 6, 
        "median_unit_cost": 14.0, 
        "quantity": 20905.0, 
        "units": "C.Y."
      }, 
      {
        "awarded_company_name": "MAMCO, INC. dba ALABBASI", 
        "awarded_unit_cost": 0.8, 
        "bid_open_date": "04/03/2019", 
        "engineer_unit_cost": 1.0, 
        "id": 1174, 
        "item_name": "ASPHALT CONCRETE EXCAVATION", 
        "item_number": 8, 
        "median_unit_cost": 1.0, 
        "quantity": 72493.0, 
        "units": "S.F."
      }, 

      ...
     ]
    }
}
```

### GET/contractor_bids/all
Retreive all available contractor bid estimates.

#### Successful Response

**Code:** `200 OK`

**Example API call:** `GET bid_items?item=excavation&item_name_not_contains=trench;basin`

**Example Content:**
```javascript
{
  "data": {
    "contractor-bids": [
      {
        "bid_open_date": "01/07/2014", 
        "company": "MAMCO, INC. dba ALABBASI", 
        "estimate": 5.6, 
        "item_name": "EXCAVATION", 
        "project_name": "PYRITE CHANNEL BYPASS, PYRITE STREET STORM DRAIN, STAGE 1", 
        "project_number": "1-0-0109-00", 
        "quantity": 6176.0, 
        "units": "C.Y."
      }, 
      {
        "bid_open_date": "01/07/2014", 
        "company": "SRD ENGINEERING, INC.", 
        "estimate": 21.8, 
        "item_name": "EXCAVATION", 
        "project_name": "PYRITE CHANNEL BYPASS, PYRITE STREET STORM DRAIN, STAGE 1", 
        "project_number": "1-0-0109-00", 
        "quantity": 6176.0, 
        "units": "C.Y."
      }, 

      ...
     ]
    }
}
```

### GET/contractor_bids?
Retrieve filtered contractor bid estimates.



#### URL Parameters

| Parameter Name       | Type   | Description                                                            |
|----------------------|--------|------------------------------------------------------------------------|
|bid_event_id          | integer| id numner for a specific project                                       |
|item                  | string | text to find within bid item name                                      |
|item_name_not_contains| string | list of semi-colon text to exclude from bid item name.                 |
|project_number        | string | project number formatted as X-X-XXXX-XX                                |  
|project_name          | string | Project Name                                                           |
|company               | string | Contractor company name                                                |
|units                 | string | Units, such as L.S., C.Y.                                              |
|date_from             | string | Include only results after this date. Must be formated as: MM/DD/YYYY  |
|date_to               | string | Include results before this date. Must be formated as: MM/DD/YYYY      |

#### Error: Invalid Date
If the provided date was not formatted as "MM/DD/YYYY" then the following error will occur

**Code:** `400 BAD REQUEST`

**Content:**
```javascript
{
  "data": {
    "message": "Invalid date format used. Please use MM/DD/YYYY."
  }, 
  "status": "fail"
}
```

#### Error: No arguments provided
At least one of the parameters above must be provided in order to retrieve a successful response.

**Code:** `400 BAD REQUEST`

**Content:**
```javascript
{
  "data": {
    "message": "No arguments provided"
  }, 
  "status": "fail"
}
```


#### Successful Response
**Code:** `200 OK`

**Example Content:**
```javascript
{
  "data": {
    "contractor-bids": [
      {
        "bid_open_date": "01/07/2014", 
        "company": "MAMCO, INC. dba ALABBASI", 
        "estimate": 5.6, 
        "item_name": "EXCAVATION", 
        "project_name": "PYRITE CHANNEL BYPASS, PYRITE STREET STORM DRAIN, STAGE 1", 
        "project_number": "1-0-0109-00", 
        "quantity": 6176.0, 
        "units": "C.Y."
      }, 
      {
        "bid_open_date": "01/07/2014", 
        "company": "SRD ENGINEERING, INC.", 
        "estimate": 21.8, 
        "item_name": "EXCAVATION", 
        "project_name": "PYRITE CHANNEL BYPASS, PYRITE STREET STORM DRAIN, STAGE 1", 
        "project_number": "1-0-0109-00", 
        "quantity": 6176.0, 
        "units": "C.Y."
      }, 

      ...
     ]
    }
}
```

### General Error Messages

#### Error: Database Error
The server had problems connecting to the database.

**Code:** `500 INTERNAL SERVER ERROR`

**Content:**
```javascript
{
  "data": {
    "message": "A database error occured."
  }, 
  "status": "fail"
}
```

#### Error: Server Error
An unknown server error has occured.

**Code:** `500 INTERNAL SERVER ERROR`

**Content:**
```javascript
{
  "data": {
    "message": "A server error occured."
  }, 
  "status": "fail"
}
```
