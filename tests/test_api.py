import json
from unittest.mock import Mock, patch

from peewee import PeeweeException

from app.api_1_0 import contractor_bids
from test_base import BaseApiTestCase

UTF_8 = 'utf-8'

class ApiTestCase(BaseApiTestCase):
    def test_hello_world(self):

        response = self.client.get('/api/v1.0/hello')
        data = response.data.decode(UTF_8)

        self.assertEqual(data, 'Hello World!')

class BidEventsApiTestCase(BaseApiTestCase):
    def test_bid_events_all(self):
        response = self.client.get('api/v1.0/bid_events/all')
        data = response.data.decode(UTF_8)

        json_data = json.loads(data) 
        bid_events = json_data['data']['bid-events']

        self.assertEqual(json_data['status'], 'success')
        self.assertEqual(len(bid_events), 3)

    def test_bid_events_filtered(self):
        response = self.client.get('api/v1.0/bid_events?id=1')

        data = response.data.decode(UTF_8)
        json_data = json.loads(data)

        bid_events = json_data['data']['bid-events']

        self.assertEqual(json_data['status'], 'success')
        self.assertEqual(
            bid_events[0]['project_number'], '2-0-0155-02')

    def test_bid_events_filtered_by_name(self):
        response = self.client.get('api/v1.0/bid_events?project_name=HEACOCK')

        data = response.data.decode(UTF_8)
        json_data = json.loads(data)
        bid_events = json_data['data']['bid-events']
        
        self.assertEqual(json_data['status'], 'success')
        self.assertEqual(len(bid_events), 1)

    def test_bid_events_filtered_by_date(self):
        response = self.client.get('api/v1.0/bid_events?date_from=01/01/2018')

        data = response.data.decode(UTF_8)
        
        json_data = json.loads(data)
        bid_events = json_data['data']['bid-events']

        self.assertEqual(json_data['status'], 'success')
        self.assertEqual(len(bid_events), 1)
        self.assertEqual(
            bid_events[0]['project_name'],
            'NORCO MDP LINE NA-1, STAGE 2 AND LATERAL NA-1A, STAGE 1')

class BidItemsApiTestCase(BaseApiTestCase):
    def test_bid_items_filtered(self):
        response = self.client.get('api/v1.0/bid_items?bid_event_id=1')

        data = response.data.decode(UTF_8)    
        data_list = json.loads(data) 
        bid_items = data_list['data']['bid-items']

        self.assertEqual(len(bid_items), 3) 

class ContractItemsApiTestCase(BaseApiTestCase):
    def test_contract_items_filtered(self):
        response = self.client.get('api/v1.0/contract_items?name=MOB')

        data = response.data.decode(UTF_8)    
        json_data = json.loads(data) 
        print(json_data)
        contract_items = json_data['data']['contract-items']
        
        self.assertEqual(len(contract_items), 1) 

class CompaniesApiTestCase(BaseApiTestCase):
    def test_contract_items_filtered(self):
        response = self.client.get('api/v1.0/companies?name=MAMCO')

        data = response.data.decode(UTF_8)    
        data_list = json.loads(data) 
        self.assertEqual(len(data_list), 1) 

class ContractorBidsApiTestCase(BaseApiTestCase):
    def test_contract_items_no_arguments(self):
        response = self.client.get('api/v1.0/contractor_bids')

        data = response.data.decode(UTF_8)
        json_data = json.loads(data)

        self.assertEqual(json_data['status'], 'fail')

    def test_contract_items_invalid_id(self):
        response = self.client.get('api/v1.0/contractor_bids?id=m')

        data = response.data.decode(UTF_8)
        json_data = json.loads(data)
        
        self.assertEqual(json_data['status'], 'fail')

    def test_contract_items_invalid_date(self):
        response = self.client.get('api/v1.0/contractor_bids?date_from=invalid_date')

        data = response.data.decode(UTF_8)
        json_data = json.loads(data)
        
        self.assertEqual(json_data['status'], 'fail')

    def test_contract_items_filtered(self):
        response = self.client.get('api/v1.0/contractor_bids?company=MAMCO')

        data = response.data.decode(UTF_8)    
        json_data = json.loads(data)

        self.assertEqual(json_data['status'], 'success')

        contract_items = json_data['data']['contractor-bids']
        
        self.assertEqual(len(contract_items), 5) 
    
    def test_contract_items_database_error(self):
        
        mock_contractor_bid = Mock(name='ContractorBid')
        
        mock_contractor_bid.contractor_bids.side_effect = PeeweeException()

        with patch.object(contractor_bids, "ContractorBid", mock_contractor_bid):
            response = self.client.get('api/v1.0/contractor_bids?company=MAMCO')

            data = response.data.decode(UTF_8)    
            json_data = json.loads(data)
            message = json_data['data']['message']

            self.assertEqual(json_data['status'], 'error')
            self.assertEqual(message, 'A database error occured.')
            

    def test_contract_items_server_error(self):
        
        mock_bid_event = Mock(name='ContractorBid')
        
        mock_bid_event.select.side_effect = Exception()

        with patch.object(contractor_bids, "ContractorBid", mock_bid_event):
            response = self.client.get('api/v1.0/contractor_bids?company=MAMCO')

            data = response.data.decode(UTF_8)    
            json_data = json.loads(data)
            
            message = json_data['data']['message']

            self.assertEqual(json_data['status'], 'error')
            self.assertEqual(message, 'A server error occured.')