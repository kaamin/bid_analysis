import csv
import datetime
import logging
import os
import unittest

import peewee
import app.model as model
from app.database_import import (load_csv, import_company, import_bid_event,
    import_contract_item, import_bid_item, import_contractor_bid,
    import_unit_conversion) 

TEST_CSV = 'tests/test_case_validated.csv'
TEST_DATABASE = peewee.SqliteDatabase(':memory:')
DATE_FORMAT = "%m/%d/%Y"

class BaseTestCase(unittest.TestCase):

    def load_test_csv(self):
        self.test_data = load_csv(TEST_CSV)
                  
    def run(self, result=None):
        #Swap the model assigned database with the temporary TEST_DATABASE
        #for the testing purposes
        models = (model.MODELS)
        with TEST_DATABASE.bind_ctx(models):
            TEST_DATABASE.create_tables(model.MODELS)

            try:
                super().run(result)
            finally:
                TEST_DATABASE.drop_tables(model.MODELS)
    
    def setUp(self):
        logger = logging.getLogger('peewee')
        #logger.addHandler(logging.StreamHandler())
        logger.setLevel(logging.DEBUG)

        logging.basicConfig(
            filename='run_log.txt', 
            level=logging.DEBUG,
            filemode='w')
        
        self.load_test_csv()
    
    def tearDown(self):
        pass

class UnitModelTestCase(BaseTestCase):
    """
    Test cases to verify operation of the unit model.
    """
    def test_initial_tables_created(self):
        """
        The 'unit' table should exist when the test runs.
        """
        model.Unit.insert_initial_data()
        self.assertTrue(model.Unit.table_exists())
    
    def test_valid_unit_types(self):
        """
        The 'unit' table should contain the following default
        units when the model's insert_initial_data method
        is run.
        """

        valid_units = [
            'ACRE',
            'C.Y.',
            'EACH',
            'L.F.',
            'LBS.',
            'PAIR',
            'S.F.',
            'S.Y.',
            'TONS'
        ]

        model.Unit.insert_initial_data()

        for valid_unit in valid_units:
            result = model.Unit.select()\
                .where(model.Unit.name==valid_unit)
            
            self.assertTrue(result.exists())
    
    def test_unit_helper_methods(self):
        """
        The model for table 'Units' helper method shoud
        return a single instance (row) for the specified 
        unit type.
        """
        model.Unit.insert_initial_data()

        self.assertEquals(model.Unit.ac().name, 'ACRE')
        self.assertEquals(model.Unit.cy().name, 'C.Y.')
        self.assertEquals(model.Unit.ea().name, 'EACH')
        self.assertEquals(model.Unit.lf().name, 'L.F.')
        self.assertEquals(model.Unit.lbs().name, 'LBS.')
        self.assertEquals(model.Unit.pair().name, 'PAIR')
        self.assertEquals(model.Unit.sf().name, 'S.F.')
        self.assertEquals(model.Unit.sy().name, 'S.Y.')
        self.assertEquals(model.Unit.tons().name, 'TONS')

class DatabaseImportTestCase(BaseTestCase):
    def setUp(self):
        super().setUp()

    def test_import_company(self):

        data = load_csv('tests/company_test.csv')
        import_company(data)

        query = model.Company.select()
        
        #pylint: disable=no-value-for-parameter
        self.assertEqual(query.count(), 7)

    def test_import_bid_event(self):
        data = load_csv('tests/company_test.csv')
        import_company(data)

        data = load_csv('tests/bid_event_test.csv')
        import_bid_event(data)

        query = model.BidEvent.select()
        
        #pylint: disable=no-value-for-parameter
        self.assertEqual(query.count(), 3)

    def test_import_contract_item(self):

        model.Unit.insert_initial_data()

        data = load_csv('tests/company_test.csv')
        import_company(data)

        data = load_csv('tests/bid_event_test.csv')
        import_bid_event(data)

        data = load_csv('tests/contract_item_test.csv')
        import_contract_item(data)

        query = model.ContractItem.select()
        
        #pylint: disable=no-value-for-parameter
        self.assertEqual(query.count(), 4)

        #There should be 2 entries for Asphalt Concrete Excavation, 
        #because there's one with units of C.Y and another with units
        #of S.F.
        query = (model.ContractItem.select()
                                   .where(
                                       model.ContractItem.name=="ASPHALT CONCRETE EXCAVATION"))
        
        #pylint: disable=no-value-for-parameter
        self.assertEqual(query.count(), 2)

    def test_import_bid_item(self):
        model.Unit.insert_initial_data()

        data = load_csv('tests/company_test.csv')
        import_company(data)

        data = load_csv('tests/bid_event_test.csv')
        import_bid_event(data)

        data = load_csv('tests/contract_item_test.csv')
        import_contract_item(data)

        data = load_csv('tests/bid_item_test.csv')
        import_bid_item(data)

        query = model.BidItem.select()
        
        #pylint: disable=no-value-for-parameter
        self.assertEqual(query.count(), 5)

        contract_item = model.ContractItem.get(
            model.ContractItem.name == 'MOBILIZATION'
        )

        query = (model.BidItem.select()
                              .where(
                                  model.BidItem.contract_item==contract_item))
        self.assertEqual(query.count(), 2)

    def test_import_unit_conversion(self):
        model.Unit.insert_initial_data()

        data = load_csv('tests/contract_item_test.csv')
        import_contract_item(data)
        
        data = load_csv('tests/unit_conversion_test.csv')
        import_unit_conversion(data)

        query = model.UnitConversion.select()
        
        #pylint: disable=no-value-for-parameter
        self.assertTrue(query.count() > 0)

        unit = model.Unit.get(model.Unit.name=='C.Y.')
        convert_to_unit = model.Unit.get(model.Unit.name=='S.F.')

        contract_item = model.ContractItem.get(
            (model.ContractItem.name == 'ASPHALT CONCRETE EXCAVATION') & (model.ContractItem.unit==unit)
        )

        unit_conversion = (model.UnitConversion.select()
                                               .join(model.ContractItem)
                                               .switch(model.UnitConversion)
                                               .join(model.Unit)
                                               .where((model.UnitConversion.contract_item==contract_item) & \
                                                      (model.UnitConversion.convert_to_unit==convert_to_unit)))
        
        #pylint: disable=no-value-for-parameter
        self.assertTrue(unit_conversion.count() > 0)

    def test_import_contractor_bid_count(self):
        model.Unit.insert_initial_data()

        data = load_csv('tests/company_test.csv')
        import_company(data)

        data = load_csv('tests/bid_event_test.csv')
        import_bid_event(data)

        data = load_csv('tests/contract_item_test.csv')
        import_contract_item(data)

        data = load_csv('tests/bid_item_test.csv')
        import_bid_item(data)

        data = load_csv('tests/contractor_bid_test.csv')
        import_contractor_bid(data)

        query = model.ContractorBid.select()

        #pylint: disable=no-value-for-parameter
        self.assertEqual(query.count(), 15)
    
    def test_query_bid_item(self):
        model.Unit.insert_initial_data()

        data = load_csv('tests/company_test.csv')
        import_company(data)

        data = load_csv('tests/bid_event_test.csv')
        import_bid_event(data)

        data = load_csv('tests/contract_item_test.csv')
        import_contract_item(data)

        data = load_csv('tests/bid_item_test.csv')
        import_bid_item(data)

        data = load_csv('tests/contractor_bid_test.csv')
        import_contractor_bid(data)

        project_number = '2-0-0155-02'

        bid_item = (model.BidItem.select()
                                 .join(model.BidEvent)
                                 .switch(model.BidItem)
                                 .join(model.ContractItem)
                                 .where((model.BidEvent.project_number == project_number) &
                                        (model.ContractItem.name == 'MOBILIZATION'))
                    ).get()
        
        query = (model.ContractorBid.select(model.ContractorBid, model.BidItem, model.Company)
                .join(model.BidItem)
                .switch(model.ContractorBid)
                .join(model.Company)
                .where(model.ContractorBid.bid_item == bid_item))

        self.assertEqual(query.count(), 3)

class BidEventUtilitieslTestCase(BaseTestCase):
    """
    Test cases to verify Bid Event model utilities.
    """

    def test_date_conversion(self):
        test_date = datetime.datetime.strptime(
            '01/31/2019',
            DATE_FORMAT)

        result = model.BidEvent.to_datetime('01/31/2019') 
        self.assertEqual(result, test_date)

        result = model.BidEvent.to_date_str(test_date)
        self.assertEqual(result, '01/31/2019')
