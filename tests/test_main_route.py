import json
from test_base import BaseViewTestCase

class MainRouteTestCase(BaseViewTestCase):
    
    def test_index(self):
        response = self.client.get('/', content_type='html/text')
        self.assertEqual(response.status_code, 200)
    
    def test_project_list(self):
        response = self.client.get('/project-list', content_type='html/text')
        self.assertEqual(response.status_code, 200)
    
    def test_bid_item_search(self):
        response = self.client.get('/bid-item-search', content_type='html/text')
        self.assertEqual(response.status_code, 200)           

    def test_lump_sum_chart_does_not_exist(self):
        response = self.client.get('/bid-item-search/does_not_exist', content_type='html/text')
        self.assertEqual(response.status_code, 404)            