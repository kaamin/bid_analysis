import logging
import os
import unittest

from flask import current_app

from app import create_app

class BaseApiTestCase(unittest.TestCase):
    """
    Defines base test class which instructs
    flask to run the test configuration.
    """
    def setUp(self):
        logger = logging.getLogger('peewee')
        #logger.addHandler(logging.StreamHandler())
        logger.setLevel(logging.DEBUG)

        logging.basicConfig(
            filename='run_log_api.txt', 
            level=logging.DEBUG,
            filemode='w')

        self.app = create_app('testing')

        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client()
    
    def tearDown(self):
        self.app_context.pop()

class BaseViewTestCase(unittest.TestCase):
    """
    Defines base test class which instructs
    flask to run the test configuration.
    """
    @classmethod
    def setUpClass(cls):
        logger = logging.getLogger('peewee')

        logger.setLevel(logging.DEBUG)

        logging.basicConfig(
            filename='run_log_view.txt', 
            level=logging.DEBUG,
            filemode='a')

    def setUp(self):
        self.app = create_app('testing')

        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client()
    
    
    def tearDown(self):
        self.app_context.pop()
        pass
