import csv
import datetime
import logging
import os
import unittest

import peewee
import app.model as model
from app.database_import import (load_csv, import_bid_event,
    import_bid_item, import_company, import_contract_item,
    import_contractor_bid)

from tests.test_db_model import (BaseTestCase, TEST_DATABASE, 
    DATE_FORMAT, TEST_CSV, load_csv)

class DatabaseImportTestCase2(BaseTestCase):
    def test_import_company(self):
        data = load_csv('tests/company_test.csv')
        import_company(data)

        q = model.Company.select()
        
        #pylint: disable=no-value-for-parameter
        self.assertTrue(q.count() > 0)

        
