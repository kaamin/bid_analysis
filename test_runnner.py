"""
Runs all tests within the tests/ directory.
"""
import unittest

if __name__ == '__main__':
    loader = unittest.TestLoader()
    
    tests = loader.discover('tests')

    runner = unittest.TextTestRunner(verbosity=3)
    result = runner.run(tests)
