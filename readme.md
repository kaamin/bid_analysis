# Bid Analysis Tool and Api
This tool and associated api provides a way to search historical bids and view the results of statistical analyses.

See the [Api Reference](api_documentation.md) for information on how to use the api.