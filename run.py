import os

from app import create_app

app = create_app(os.getenv('FLASK_CONFIG') or 'default')

if __name__ == '__main__':
    url = 'http://127.0.0.1:5000/'

    app.run(debug=True, host='0.0.0.0')

